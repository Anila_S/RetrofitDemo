package fragments;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.example.user.retrofitdemo.R;

import models.ActorResults;
import rest.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

/**
 * Created by user on 1/10/18.
 */

public class ActorsFragment {
    private void getActorsList() {
        final ProgressDialog loading = ProgressDialog.show(getActivity(),getContext().getString(R.string.loading_title),getContext().getString(R.string.loading_please_wait),false,false);

        Call<ActorResults> call = ApiClient.get().getActors();

        call.enqueue(new Callback<ActorResults>() {
            @Override
            public void onFailure(Call<ActorResults> call, Throwable t) {
                Log.d("APIPlug", "Error Occured: " + t.getMessage());

                loading.dismiss();
            }

            @Override
            public void onResponse(Call<ActorResults> call, Response<ActorResults> response) {
                Log.d("APIPlug", "Successfully response fetched" );

                loading.dismiss();

                actors=response.body().results;

                if(actors.size()>0) {
                    showList();
                }else{
                    Log.d("APIPlug", "No item found");
                }
            }
        });
    }

    //Our method to show list
    private void showList() {
        Log.d("APIPlug", "Show List");

        ActorAdapter adapter = new ActorAdapter(getActivity(), actors);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Actor clickedObj = (Actor) parent.getItemAtPosition(position);

                //If you have detail activity about your object define in here
            /*
            Intent detail = new Intent(getContext(), ActorDetail.class);
            detail.putExtra("actorObject", clickedObj);
            startActivity(detail);
            */
            }});

    }

}

package rest;

import models.ActorResults;
import models.CountryResults;
import models.FilmResults;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by user on 1/10/18.
 */


    public interface APIPlug {


        @GET("actor")
        Call<ActorResults> getActors();

        @GET("country")
        Call<CountryResults> getCountries();

        @GET("film")
        Call<FilmResults> getFilms();


    }

